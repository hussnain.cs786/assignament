package com.example.assigment2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ExampleAdapter extends RecyclerView.Adapter<ExampleAdapter.ExampleViewHolder> {

    private Context mContext ;
    private ArrayList<Exampleitem> mExampleList ;

    public ExampleAdapter(Context context , ArrayList<Exampleitem> examplelist){

        mContext = context ;
        mExampleList = examplelist ;
    }


    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(mContext).inflate(R.layout.item_list,parent,false);
        return new ExampleViewHolder(v) ;
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {

        Exampleitem currentitem = mExampleList.get(position);

        String imageUrl = currentitem.getmImageUrl();
        String likeCount = currentitem.getLikeCount() ;

        holder.mTextViewLikes.setText("Likes:" + likeCount);
        Picasso.get().load(imageUrl).fit().centerInside().into(holder.mImageview);


    }

    @Override
    public int getItemCount() {
        return mExampleList.size();
    }

    public class ExampleViewHolder extends RecyclerView.ViewHolder{

        public ImageView mImageview ;
        public TextView mTextViewLikes   ;

        public ExampleViewHolder(@NonNull View itemView) {
            super(itemView);

            mImageview = itemView.findViewById(R.id.image_view);
            mTextViewLikes = itemView.findViewById(R.id.text_view_likes);
        }
    }
}
