package com.example.assigment2;

public class Exampleitem {

    private String mImageUrl ;
    private String mLikes ;

    public  Exampleitem (String imageUrl , String likes){

        mImageUrl = imageUrl ;
        mLikes = likes ;

    }

    public String getmImageUrl(){
        return  mImageUrl ;
    }

    public String getLikeCount(){
        return  mLikes ;
    }
}
