package com.example.assigment2;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView mRecyclerview ;
    private ExampleAdapter mExampleAdapter ;
    private ArrayList<Exampleitem> mExamplelist ;
    private RequestQueue mRequestQueue ;

    public SwipeRefreshLayout swipeRefreshLayout ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerview = findViewById(R.id.recycle_view);
        mRecyclerview.setHasFixedSize(true);
        mRecyclerview.setLayoutManager(new LinearLayoutManager(this));

        mExamplelist = new ArrayList<>();
        mRequestQueue = Volley.newRequestQueue(this);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.cologreen);
        swipeRefreshLayout.setOnRefreshListener(this);
        parseJSON();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayoutManager layoutManager = (LinearLayoutManager)mRecyclerview.getLayoutManager();
                layoutManager.scrollToPositionWithOffset(0,0);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void parseJSON(){

        String url = "http://pastebin.com/raw/wgkJgazE";

        final StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            swipeRefreshLayout.setRefreshing(true);
                            JSONArray jsonArray = new JSONArray(response);
                            mExamplelist.clear();
                            for (int i = 0 ; i < jsonArray.length(); i++){

                                JSONObject cat = jsonArray.getJSONObject(i);


                                String likecount = cat.getString("likes");
                                String photo = cat.getString("user");
                                JSONObject j = new JSONObject(photo);
                                JSONObject image = new JSONObject(j.getString("profile_image"));
                                String small = image.getString("large");

                                mExamplelist.add(new Exampleitem(small,likecount));

                            }

                            mExampleAdapter = new ExampleAdapter(MainActivity.this,mExamplelist);
                            mRecyclerview.setAdapter(mExampleAdapter);

                            swipeRefreshLayout.setRefreshing(false);

                        } catch (JSONException e) {

                            swipeRefreshLayout.setRefreshing(false);

                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Log.e("ERROR",error.toString());
            }
        });

        mRequestQueue.add(request);
    }

    @Override
    public void onRefresh() {

        parseJSON();

    }
}
